#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>


int BtoD(int[][16], int, int, int);

int compteur(int,int);

void iMemory(int[][16]);

void dMemory(int[][16], int);

void reg(int[][16]);

void regwb(int[][16],int );

void stor(int[][16], int, int);

int fetch(int[][16],int[][4], int);

int decode(int[][16], int[][4], int, int[][16]);

int execute(int[][16], int[][4], int, int[][16]);

int memory(int[][16], int[][4], int, int);

int wback(int[][16], int[][4], int, int, int[][16]);

void test(int, int[][16]);
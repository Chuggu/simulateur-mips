#include "fonctions.h"

int main()
{
    int i,j=0,d,pc = 0;
    char c;
    int tab2D[500][16] = {0}; // un tableau de 500 lignes de 16bits   

    FILE* fichier = NULL;
    fichier = fopen("test.txt","r");

    if (fichier == NULL)
    {
        printf("Impossible d'ouvrir le fichier.. \n");
        exit(0);
    }

    while(c!=EOF)
    {
        for(i = 0; i<16;i++)
        {
            c = fgetc(fichier);
            d = atoi(&c);
            tab2D[j][i] = d;      
        }
        fseek(fichier,1,SEEK_CUR);
        j++;
    }

    test(pc,tab2D);
    printf("\n");
    return 0;
}
# Simulateur MIPS

Parmis les instructions que nous allons utiliser pour ce projet, nous retrouvons 4 types: 

* opcode (4 bits) | reg 1 (4 bits) | reg 2 (4 bits) | reg 3 (4 bits)
* opcode (4 bits) | reg 1 (4 bits) | reg 2 (4 bits) | immediate (4 bits)
* opcode (4 bits) | immediate (12 bits)
* opcode (4 bits) | 0 (12 bits)

Chaque opcode défini une instruction spécifique:

* add :                     0001
* substract :               0010
* add immediate :           0011
* multiply :                0100
* load word :               0101
* store word :              0110
* branch on equal :         0111
* branch on not equal :     1000
* jump :                    1001
* exit :                    1111

Le tableau inst est de dimensions 250 par 4 , car on nous dit que la mémoire totale fait 500 lignes et que seule la première moitié peut contenir des instruction, donc PC ne sera jamais supérieur à 250. 

Dans ce dossier, il y a une première version se compilant avec makefile sans gérer les dépendance entre registre, et une autre version se compilant en utilisant 'gcc V0test.c -lm' où j'ai essayer de gérer ses dépendance, mais il reste un soucis, c'est pour cela que j'ai mis la première version sous makefile et non celle ci.

Ce projet est aussi disponible sur mon gitlab : https://gitlab.com/Chuggu/simulateur-mips .
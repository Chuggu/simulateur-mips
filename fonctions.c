#include "fonctions.h"

int BtoD(int tab2D[500][16], int m, int n, int ligne)
{
    int entier = 0, i, j=0;

    for(i=n-1;i>m-1;i--)
    {
        entier = entier + tab2D[ligne][i]*pow(2,j);
        j++;
    }
    return entier;
}

//================================================================================

int compteur(int pc,int n)
{
    pc = pc + n;
    printf("\nIncrémentation du compteur de programme : PC = %d\n",pc);
    return pc;
}

//================================================================================

void iMemory(int tab2D[500][16])
{
    int i, j;
    printf("\t           MEMORY\n");
    printf("\t----------------------------\n");

    for(j=0;j<20;j++)
    {
        if(j<9)
        {
            printf("\t|   %d  -  ",j);
        }
        if(j>=9 && j<12)
        {
            printf("\t|   .  -  ");
        }
        if(j>=12)
        {
            printf("\t| %d  -  ",j+481);
        }
        for(i = 0; i<16;i++)
        {
            printf("%d",tab2D[j][i]);
        }
        printf(" |");
        printf("\n");
    }

    printf("\t----------------------------\n\n");
}

//================================================================================

void dMemory(int tab2D[500][16], int tmp)
{
    int i, j;
    printf("\t           MEMORY\n");
    printf("\t----------------------------\n");

    for(j=0;j<20;j++)
    {
        if(j == tmp)
        {
            if(j<9)
            {
                printf("\t|   \033[32m%d\033[00m  -  ",j);
            }
            if(j>=9 && j<12)
            {
                printf("\t|   \033[32m.\033[00m  -  ");
            }
            if(j>=12)
            {
                printf("\t| \033[32m%d\033[00m  -  ",j+481);
            }
            for(i = 0; i<16;i++)
            {
                printf("\033[32m%d\033[00m",tab2D[j][i]);
            }
            printf(" |");
            printf("\n");
        }
        else
        {
            if(j<9)
            {
                printf("\t|   %d  -  ",j);
            }
            if(j>=9 && j<12)
            {
                printf("\t|   .  -  ");
            }
            if(j>=12)
            {
                printf("\t| %d  -  ",j+481);
            }
            for(i = 0; i<16;i++)
            {
                printf("%d",tab2D[j][i]);
            }
            printf(" |");
            printf("\n");
        }
    }

    printf("\t----------------------------\n\n");
}

//================================================================================

void reg(int registres[16][16])
{
int i, j;
    printf("\t   TABLEAU DE REGISTRES\n");
    printf("\t---------------------------\n");

    for(j=0;j<16;j++)
    {
        if(j<10)
        {
            printf("\t|  %d  -  ",j);
        }
        else
        {
            printf("\t| %d  -  ",j);
        }
        for(i = 0; i<16;i++)
        {
            printf("%d",registres[j][i]);
        }
        printf(" |");
        printf("\n");
    }

    printf("\t---------------------------\n\n");
}

//================================================================================

void regwb(int registres[16][16],int adresse)
{
int i, j;
    printf("\t   TABLEAU DE REGISTRES\n");
    printf("\t---------------------------\n");

    for(j=0;j<16;j++)
    {
        if(j == adresse)
        {
            if(j<10)
            {
                printf("\t|  \033[32m%d\033[00m  -  ",j);
            }
            else
            {
                printf("\t| \033[32m%d\033[00m  -  ",j);
            }
            for(i = 0; i<16;i++)
            {
                printf("\033[32m%d\033[00m",registres[j][i]);
            }
            printf(" |");
            printf("\n");
        }
        else
        {
            if(j<10)
            {
                printf("\t|  %d  -  ",j);
            }
            else
            {
                printf("\t| %d  -  ",j);
            }
            for(i = 0; i<16;i++)
            {
                printf("%d",registres[j][i]);
            }
            printf(" |");
            printf("\n");
        }
    }

    printf("\t---------------------------\n\n");
}
//================================================================================

void stor(int tab2D[500][16], int tmp, int val)
{
    int i,j,ad,k = 15;
    int t[16] = {0};

    ad = abs(val);

    for(j = 0; ad > 0; j++)
    {
        t[j] = ad % 2;
        ad = ad / 2;
    }

    for(i=0; i<16; i++)
    {
        tab2D[tmp][k] = t[i];
        k=k-1;
    }

}


//================================================================================

int fetch(int tab2D[500][16],int inst[250][4], int pc)
{
    if(inst[pc][0]==0)
    {
        printf("Il n'y a plus d'instruction à Fetch\n");
        pc = compteur(pc,1);
        return pc;
    }

    printf("\n--------------------------------------------------\n");
    printf("\n\t============================\n");
    printf("\t| FETCH DE L'INSTRUCTION %d |\n",pc);
    printf("\t============================\n\n");
    
    iMemory(tab2D);
  
    printf("L'instruction %d est : ", pc);
    for(int i=0;i<16;i++)
    {
        printf(" %d",tab2D[pc][i]);
    }
    
    pc = compteur(pc,1);

    return pc;
    
}

//================================================================================

int decode(int tab2D[500][16], int inst[250][4], int pc, int registres[16][16])
{
    if(inst[pc][0]==0)
    {
        printf("Il n'y a plus d'instruction à Decode\n");
        return 0;
    }

    int adresse;
    adresse = BtoD(tab2D,4,16,pc);

    printf("\n--------------------------------------------------\n");
    printf("\n\t=============================\n");
    printf("\t| DECODE DE L'INSTRUCTION %d |\n",pc);
    printf("\t=============================\n\n");

    reg(registres);

    switch (inst[pc][0])
    {
    case 1:
        printf("Instruction %d : add\n",pc);
        printf("Détail après conversion en décimal : \n");
        printf("\tOpcode :             %d\n", inst[pc][0]);
        printf("\tPremier registre :   %d\n", inst[pc][1]);
        printf("\tDeuxième registre :  %d\n", inst[pc][2]);
        printf("\tTroisième registre : %d\n", inst[pc][3]);
        break;
        
    case 2:
        printf("Instruction %d : substract\n",pc);
        printf("Détail après conversion en décimal : \n");
        printf("\tOpcode :             %d\n", inst[pc][0]);
        printf("\tPremier registre :   %d\n", inst[pc][1]);
        printf("\tDeuxième registre :  %d\n", inst[pc][2]);
        printf("\tTroisième registre : %d\n", inst[pc][3]);
        break;

    case 3:
        printf("Instruction %d : add immediate\n",pc);
        printf("Détail après conversion en décimal : \n");
        printf("\tOpcode :             %d\n", inst[pc][0]);
        printf("\tPremier registre :   %d\n", inst[pc][1]);
        printf("\tDeuxième registre :  %d\n", inst[pc][2]);
        printf("\tImmediate :          %d\n", inst[pc][3]);
        break;

    case 4:
        printf("Instruction %d : multiply\n",pc);
        printf("Détail après conversion en décimal : \n");
        printf("\tOpcode :             %d\n", inst[pc][0]);
        printf("\tPremier registre :   %d\n", inst[pc][1]);
        printf("\tDeuxième registre :  %d\n", inst[pc][2]);
        printf("\tTroisième registre : %d\n", inst[pc][3]);
        break;

    case 5:
        printf("Instruction %d : load word\n",pc);
        printf("Détail après conversion en décimal : \n");
        printf("\tOpcode :             %d\n", inst[pc][0]);
        printf("\tPremier registre :   %d\n", inst[pc][1]);
        printf("\tDeuxième registre :  %d\n", inst[pc][2]);
        printf("\tImmediate :          %d\n", inst[pc][3]);
        break;

    case 6:
        printf("Instruction %d : store word\n",pc);
        printf("Détail après conversion en décimal : \n");
        printf("\tOpcode :             %d\n", inst[pc][0]);
        printf("\tPremier registre :   %d\n", inst[pc][1]);
        printf("\tDeuxième registre :  %d\n", inst[pc][2]);
        printf("\tImmediate :          %d\n", inst[pc][3]);
        break;

    case 7:
        printf("Instruction %d : branch on equal\n",pc);
        printf("Détail après conversion en décimal : \n");
        printf("\tOpcode :             %d\n", inst[pc][0]);
        printf("\tPremier registre :   %d\n", inst[pc][1]);
        printf("\tDeuxième registre :  %d\n", inst[pc][2]);
        printf("\tImmediate :          %d\n", inst[pc][3]);
        break;

    case 8:
        printf("Instruction %d : branch on not equal\n",pc);
        printf("Détail après conversion en décimal : \n");
        printf("\tOpcode :             %d\n", inst[pc][0]);
        printf("\tPremier registre :   %d\n", inst[pc][1]);
        printf("\tDeuxième registre :  %d\n", inst[pc][2]);
        printf("\tImmediate :          %d\n", inst[pc][3]);
        break;

    case 9:
        printf("Instruction %d : jump\n",pc);
        printf("Détail après conversion en décimal : \n");
        printf("\tOpcode :             %d\n", inst[pc][0]);
        printf("\tAdresse :            %d\n", adresse);
        break;
    
    case 15:
        printf("Instruction %d : exit\n",pc);
        printf("Détail après conversion en décimal : \n");
        printf("\tOpcode :             %d\n", inst[pc][0]);
        break;

    default:
        printf("Opcode invalide, veuillez vérifiez votre fichier.\n");
        break;
    }
    
    return 0;
}

//================================================================================

int execute(int tab2D[500][16], int inst[250][4], int pc, int registres[16][16])
{
    int tmp = 0,tmp1,tmp2;

    if(inst[pc][0]==0)
    {
        printf("Il n'y a plus d'instruction à Execute\n");
        return 0;
    }
    printf("\n--------------------------------------------------\n");
    printf("\n\t==============================\n");
    printf("\t| EXECUTE DE L'INSTRUCTION %d |\n",pc);
    printf("\t==============================\n\n");

    switch (inst[pc][0])
    {
    case 1:
        printf("Instruction %d : add\n",pc);
        tmp1 = BtoD(registres,0,16,(inst[pc][2]));
        tmp2 = BtoD(registres,0,16,(inst[pc][3]));
        tmp = tmp1 + tmp2;
        break;
        
    case 2:
        printf("Instruction %d : substract\n",pc);
        tmp1 = BtoD(registres,0,16,(inst[pc][2]));
        tmp2 = BtoD(registres,0,16,(inst[pc][3]));
        tmp = tmp1 - tmp2;
        break;

    case 3:
        printf("Instruction %d : add immediate\n",pc);
        tmp1 = BtoD(registres,0,16,(inst[pc][2]));
        tmp2 = inst[pc][3];
        tmp = tmp1 + tmp2;
        break;

    case 4:
        printf("Instruction %d : multiply\n",pc);
        tmp1 = BtoD(registres,0,16,(inst[pc][2]));
        tmp2 = BtoD(registres,0,16,(inst[pc][3]));
        tmp = tmp1 * tmp2;
        break;

    case 5:
        printf("Instruction %d : load word\n",pc);
        tmp1 = BtoD(registres,0,16,(inst[pc][2]));
        tmp2 = inst[pc][3];
        tmp = tmp1 + tmp2;
        break;

    case 6:
        printf("Instruction %d : store word\n",pc);
        tmp1 = BtoD(registres,0,16,(inst[pc][2]));
        tmp2 = inst[pc][3];
        tmp = tmp1 + tmp2;
        break;

    case 7:
        printf("Instruction %d : branch on equal\n",pc);
        tmp1 = BtoD(registres,0,16,(inst[pc][1]));
        tmp2 = BtoD(registres,0,16,(inst[pc][2]));
        if(tmp1 == tmp2)
        {
            pc = compteur(pc,inst[pc][3]);
            return pc;
        }
        break;

    case 8:
        printf("Instruction %d : branch on not equal\n",pc);
        tmp1 = BtoD(registres,0,16,(inst[pc][1]));
        tmp2 = BtoD(registres,0,16,(inst[pc][2]));
        if(tmp1 != tmp2)
        {
            pc = compteur(pc,inst[pc][3]);
            return pc;
        }
        break;

    case 9:
        printf("Instruction %d : jump\n",pc);
        tmp = BtoD(tab2D,4,16,pc);
        pc = compteur(pc,tmp-pc);
        return pc;
        break;
    
    case 15:
        printf("Instruction %d : exit\n",pc);
        printf("Cette instruction indique la fin du programme.\n");
        exit(0);
        break;

    default:
        printf("Opcode invalide, aucune exécution effectuée.\n");
        break;
    }
    printf("\n");
    printf("\nTMP = %d\nTMP1 = %d\nTMP2 = %d\n",tmp,tmp1,tmp2);
    return tmp;
}

//================================================================================

int memory(int tab2D[500][16], int inst[250][4], int pc, int tmp)
{
    int val,tmpwb;
    val = inst[pc][1];
    if(inst[pc][0]==0)
    {
        printf("Il n'y a plus d'instruction à Memory\n");
        return 0;
    }
    printf("\n--------------------------------------------------\n");
    printf("\n\t=============================\n");
    printf("\t| MEMORY DE L'INSTRUCTION %d |\n",pc);
    printf("\t=============================\n\n");
    
    if(inst[pc][0]==6) // il faut ecrire a l'adresse memoire tab2D[tmp][i], la valeur val en binaire
    {
        stor(tab2D,tmp,val);
        dMemory(tab2D,tmp);
    }
    else
    {
        iMemory(tab2D);
    }
    
    tmpwb = tmp;

    return tmpwb;
}

//================================================================================

int wback(int tab2D[500][16], int inst[250][4], int pc, int tmpwb, int registres[16][16])
{
    int val, adresse,tmp;
    val = tmpwb;
    adresse = inst[pc][1];
    
    if(inst[pc][0]==0)
    {
        printf("Il n'y a plus d'instruction à Wback\n");
        exit(0);
    }
    printf("\n--------------------------------------------------\n");
    printf("\n\t============================\n");
    printf("\t| WBACK DE L'INSTRUCTION %d |\n",pc);
    printf("\t============================\n\n");
    //pour les fonction 1,2,3,4,5, on utilise la fonction stor et on ecris dans le registre [inst[pc][1]]
    switch (inst[pc][0])
    {
    case 1://il faut stocker dans le tab de registre a l'adresse inst[pc][1] la valeur tmp
        stor(registres,adresse,val);
        regwb(registres,adresse);
        break;

    case 2:
        stor(registres,adresse,val);
        regwb(registres,adresse);
        break;

    case 3:
        stor(registres,adresse,val);
        regwb(registres,adresse);
        break;

    case 4:
        stor(registres,adresse,val);
        regwb(registres,adresse);
        break;

    case 5:
        tmp = BtoD(tab2D,0,16,val);
        stor(registres,adresse,tmp);
        regwb(registres,adresse);
        break;

    default:
        reg(registres);
        break;
    }

    return 0;
}

//================================================================================

void test(int pc, int tab2D[500][16])
{
    int i,j,choix, tmp,tmpwb;
    int inst[250][4], registres[16][16] = {0};

    registres[1][11]=1;  // Le premier registre a pour valeur 16
    registres[2][14]=1;  // Le deuxième registre a pour valeur 2

    while(1)
    {
        for(i=0;i<4;i++)
        {
            j = i*4;
            inst[pc][i]= BtoD(tab2D,j,j+4,pc);
        }

        choix = 0;
        printf("\n\n====================================================================================\n");
        printf("\nEntrez '1' pour passez au cycle d'horloge suivant ou un autre caractère pour quitter.\n");
        scanf("%d",&choix);

        if(choix == 1)
        {
            printf("Cycle d'horloge suivant..\n");//sleep(1);
            switch (pc)
            {
            case 0:
                pc = fetch(tab2D,inst,pc);
                break;

            case 1:
                decode(tab2D,inst,pc-1,registres);
                pc = fetch(tab2D,inst,pc);
                break;

            case 2:
                if(inst[pc-2][0] == 7 || inst[pc-2][0] == 8 || inst[pc-2][0] == 9)
                {
                    pc = execute(tab2D,inst,pc-2,registres);
                    decode(tab2D,inst,pc-1,registres);
                    fetch(tab2D,inst,pc);
                }
                else
                {
                    tmp = execute(tab2D,inst,pc-2,registres);
                    decode(tab2D,inst,pc-1,registres);
                    pc = fetch(tab2D,inst,pc);
                }
                break;

            case 3:
                tmpwb = memory(tab2D,inst,pc-3,tmp);
                if(inst[pc-2][0] == 7 || inst[pc-2][0] == 8 || inst[pc-2][0] == 9)
                {
                    pc = execute(tab2D,inst,pc-2,registres);
                    decode(tab2D,inst,pc-1,registres);
                    fetch(tab2D,inst,pc);
                }
                else
                {
                    tmp = execute(tab2D,inst,pc-2,registres);
                    decode(tab2D,inst,pc-1,registres);
                    pc = fetch(tab2D,inst,pc);
                }
                break;
            
            default:
                wback(tab2D,inst,pc-4,tmpwb,registres);
                tmpwb = memory(tab2D,inst,pc-3,tmp);
                if(inst[pc][0] == 7 || inst[pc][0] == 8 || inst[pc][0] == 9)
                {
                    pc = execute(tab2D,inst,pc-2,registres);
                    decode(tab2D,inst,pc-1,registres);
                    fetch(tab2D,inst,pc);
                }
                else
                {
                    tmp = execute(tab2D,inst,pc-2,registres);
                    decode(tab2D,inst,pc-1,registres);
                    pc = fetch(tab2D,inst,pc);
                }
                break;
            }
        }
        else
        {
            printf("Vous avez décidé de quitter.\n");
            exit(0);
        }
    }


}